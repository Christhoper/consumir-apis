package com.example.apiweather;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView tvCiudad, tvSantiago;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvCiudad = (TextView) findViewById(R.id.tvCiudad);
        this.tvSantiago = (TextView) findViewById(R.id.tvSantiago);

        String santiagoUrl = "http://api.openweathermap.org/data/2.5/weather?lat=-33.4726900&lon=-70.6472400&appid=eaaa105aec5ad879a1b97a661f65ee9d&units=metric";

        StringRequest santiago = new StringRequest(
                Request.Method.GET,
                santiagoUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);

                            String ciudadSantiago = respuestaJSON.getString("name");

                            JSONObject celsiusJSON = respuestaJSON.getJSONObject("main");
                            double celsius = celsiusJSON.getDouble("temp");

                            tvSantiago.setText("ciudad" + ciudadSantiago);
                            Log.d("VALOR_REQUEST", "ESTE ES EL VALOR DE LA CIUDAD: " + ciudadSantiago);
                            Log.d("VALOR_REQUEST", "ESTE ES EL VALOR DE LOS GRADOS: " + celsius + "°");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Fallo
                    }
                }
        );

        RequestQueue listaEsperaSantiago = Volley.newRequestQueue(getApplicationContext());
        listaEsperaSantiago.add(santiago);

        String chillanUrl = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6066399&lon=-72.1034393&appid=eaaa105aec5ad879a1b97a661f65ee9d&units=metric";
        StringRequest chillan = new StringRequest(
                Request.Method.GET,
                chillanUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);

                            String ciudadChillan = respuestaJSON.getString("name");

                            JSONObject celsiusJSON = respuestaJSON.getJSONObject("main");
                            double celsius = celsiusJSON.getDouble("temp");

                            tvCiudad.setText("ciudad" + ciudadChillan);
                            Log.d("VALOR_REQUEST", "ESTE ES EL VALOR DE LA CIUDAD: " + ciudadChillan);
                            Log.d("VALOR_REQUEST", "ESTE ES EL VALOR DE LOS GRADOS: " + celsius + "°");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Fallo
                    }
                }
        );

        RequestQueue listaEsperaChillan = Volley.newRequestQueue(getApplicationContext());
        listaEsperaChillan.add(chillan);


        String concepcionUrl = "http://api.openweathermap.org/data/2.5/weather?lat=-36.8269882&lon=-73.0497665&appid=eaaa105aec5ad879a1b97a661f65ee9d&units=metric";
        StringRequest concepcion = new StringRequest(
                Request.Method.GET,
                concepcionUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);

                            String ciudadConcepcion = respuestaJSON.getString("name");

                            JSONObject celsiusJSON = respuestaJSON.getJSONObject("main");
                            double celsius = celsiusJSON.getDouble("temp");

                            tvCiudad.setText("ciudad" + ciudadConcepcion);
                            Log.d("VALOR_REQUEST", "ESTE ES EL VALOR DE LA CIUDAD: " + ciudadConcepcion);
                            Log.d("VALOR_REQUEST", "ESTE ES EL VALOR DE LOS GRADOS: " + celsius + "°");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Fallo
                    }
                }
        );

        RequestQueue listaEsperaConcepcion = Volley.newRequestQueue(getApplicationContext());
        listaEsperaConcepcion.add(concepcion);


        //Wenaaaaaaa

    }
}

